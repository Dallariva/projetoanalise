package com.maven.trabalhopatterns.template;

public class Pacote1 extends Opcionais{
    
    private static Pacote1 pacote1;
    
    private Pacote1(){
    }
    
    public static Pacote1 getInstance(){
        if(pacote1 == null){
            pacote1 = new Pacote1();
        }
        return pacote1;
    }

    @Override
    public String listaDeOpcionais() {
        return "Vidro elétrico, trava elétrica, alarme.";
    }

    @Override
    public String nome() {
        return "Pacote 1";
    }
}
