package com.maven.trabalhopatterns.template;

public class Basico extends Opcionais{

    private static Basico basico;
    
    private Basico(){
    }
    
    public static Basico getInstance(){
        if(basico == null){
            basico = new Basico();
        }
        return basico;
    }
    
    @Override
    public String listaDeOpcionais() {
        return "Sem opcicionais";
    }

    @Override
    public String nome() {
        return "Básico";
    }
}
