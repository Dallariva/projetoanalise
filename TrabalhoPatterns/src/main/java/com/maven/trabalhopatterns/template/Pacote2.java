package com.maven.trabalhopatterns.template;

public class Pacote2 extends Opcionais{

    private static Pacote2 pacote2;
    
    private Pacote2(){
    }
    
    public static Pacote2 getInstance(){
        if(pacote2 == null){
            pacote2 = new Pacote2();
        }
        return pacote2;
    }
    
    @Override
    public String listaDeOpcionais() {
        return "Direção hidráulica, ABS, AIR BAG";
    }

    @Override
    public String nome() {
        return "Pacote 3";
    }
    
}
