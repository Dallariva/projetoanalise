package com.maven.trabalhopatterns.template;

public class Pacote3 extends Opcionais{

    private static Pacote3 pacote3;
    
    private Pacote3(){
    }
    
    public static Pacote3 getInstance(){
        if(pacote3 == null){
            pacote3 = new Pacote3();
        }
        return pacote3;
    }
    
    @Override
    public String listaDeOpcionais() {
        return "Ar-condicionado, central de multimídia, teto solar";
    }

    @Override
    public String nome() {
        return "Pacote 3";
    }
    
}
