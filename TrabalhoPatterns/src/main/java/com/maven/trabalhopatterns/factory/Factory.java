package com.maven.trabalhopatterns.factory;

public class Factory {    
    
    private static Factory factory;
    
    private Factory(){
        
    }
    
    public static Factory getInstance(){
        if(factory == null){
            return factory = new Factory();
        }
        
        return factory;
    }
    
    public Montadora criarNovoCarro(String nome){
        Gol gol;
        Golf golf;
        Duster duster;
        Uno uno;
        
        try{
            if(nome.equalsIgnoreCase("GOL")){
                return gol = new Gol();
            }
            
            if(nome.equalsIgnoreCase("GOLF")){
                return golf = new Golf();
            }
            
            if(nome.equalsIgnoreCase("DUSTER")){
                return duster = new Duster();
            }
            
            if(nome.equalsIgnoreCase("UNO")){
                return uno = new Uno();
            }
        }
        
        catch(Exception ex){
            System.out.println(ex);
        }
        
        return null;
    }
    
}
