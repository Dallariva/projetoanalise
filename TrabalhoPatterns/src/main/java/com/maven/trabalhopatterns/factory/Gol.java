package com.maven.trabalhopatterns.factory;

public class Gol implements Montadora{

    @Override
    public void nomeCarro() {
        System.out.println("Gol");
    }

    @Override
    public void categoria() {
        System.out.println("Popular");
    }

    @Override
    public void marcaCarro() {
        System.out.println("VW");
    }
    
}
