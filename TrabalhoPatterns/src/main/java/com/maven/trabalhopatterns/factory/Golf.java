package com.maven.trabalhopatterns.factory;

public class Golf implements Montadora{

    @Override
    public void nomeCarro() {
        System.out.println("Golf");
    }

    @Override
    public void categoria() {
        System.out.println("Esportivo");
    }

    @Override
    public void marcaCarro() {
        System.out.println("VW");
    }
    
}
