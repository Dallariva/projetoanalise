package com.maven.trabalhopatterns.factory;
import com.maven.trabalhopatterns.*;

public interface Montadora {
    
    public void nomeCarro();
    
    public void categoria();
    
    public void marcaCarro();
}
