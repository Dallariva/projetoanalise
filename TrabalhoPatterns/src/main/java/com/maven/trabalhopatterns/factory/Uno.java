package com.maven.trabalhopatterns.factory;

public class Uno implements Montadora{

    @Override
    public void nomeCarro() {
        System.out.println("Uno");
    }

    @Override
    public void categoria() {
        System.out.println("Popular");
    }

    @Override
    public void marcaCarro() {
        System.out.println("Fiat");
    }
    
}
