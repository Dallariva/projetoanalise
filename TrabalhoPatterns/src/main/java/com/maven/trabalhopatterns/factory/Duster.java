package com.maven.trabalhopatterns.factory;

public class Duster implements Montadora{

    @Override
    public void nomeCarro() {
        System.out.println("Duster");
    }
    @Override
    public void categoria() {
        System.out.println("Utilitário");
    }

    @Override
    public void marcaCarro() {
        System.out.println("Renault");
    }
    
}
