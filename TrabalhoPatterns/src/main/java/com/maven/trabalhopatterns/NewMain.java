package com.maven.trabalhopatterns;
import com.maven.trabalhopatterns.template.*;
import com.maven.trabalhopatterns.factory.*;

import com.maven.trabalhopatterns.factory.Factory;

public class NewMain {

    public static void main(String[] args) {
        Factory f = Factory.getInstance();
        Pacote2 p2 = Pacote2.getInstance();
        Montadora m = f.criarNovoCarro("Gol");
        m.nomeCarro();
        m.marcaCarro();
        m.categoria();
        p2.nome();
        p2.listaDeOpcionais();
        
        Factory f1 = Factory.getInstance();
        Basico b = Basico.getInstance();
        Montadora m1 = f1.criarNovoCarro("Uno");
        m1.nomeCarro();
        m1.marcaCarro();
        m1.categoria();
        b.nome();
        b.listaDeOpcionais();
    }
    
}
